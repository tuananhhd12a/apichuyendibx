﻿namespace APIBenXe.Model2
{
    public class ChuyenHoanThanh
    {
        /// <summary>
        /// Mã chuyến đi
        /// </summary>
        public string MaChuyenDi { get; set; } = null!;
        /// <summary>
        /// Tên trạng thái chuyến đi
        /// </summary>
        public string TenNhaXe { get; set; } = null!;
        /// <summary>
        /// Số điện thoại nhà xe
        /// </summary>
        public string SoDienThoaiNhaXe { get; set; } = null!;
        /// <summary>
        /// Biển kiểm soát
        /// </summary>
        public string BienKiemSoat { get; set; } = null!;
        /// </summary>
        /// Họ tên nhân viên
        /// </summary>
        public string HoTenNhanVien { get; set; } = null!;
        /// Ten Ben Di
        /// </summary>
        public string Bendi { get; set; } = null;
        /// <summary>
        /// Ten Ben Den
        /// </summary>
        public string Benden { get; set; } = null;
        /// <summary>
        /// Thời gian khởi hành
        /// </summary>
        /// <summary>

        public DateTime? ThoiGianKhoiHanh { get; set; }
        /// <summary>
        /// Thời gian kết thúc
        /// </summary>
        public DateTime? ThoiGianKetThuc { get; set; }
        /// <summary>
        /// Lý do hủy chuyến đi
        /// </summary>

    }
}
