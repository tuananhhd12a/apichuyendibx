﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class Chuyendi
    {
        public Chuyendi()
        {
            ChuyendiNhanviens = new HashSet<ChuyendiNhanvien>();
            MuaVes = new HashSet<MuaVe>();
        }

        public Guid IdChuyenDi { get; set; }
        public Guid? IdTuyenDuong { get; set; }
        public Guid? IdTrangThai { get; set; }
        public string? MaChuyenDi { get; set; }
        public DateTime? ThoiGianKhoiHanh { get; set; }
        public DateTime? ThoiGianKetThuc { get; set; }
        public decimal? GiaVe { get; set; }
        public string? LyDoHuy { get; set; }
        public DateTime? ThoiGianHuy { get; set; }

        public virtual Trangthai? IdTrangThaiNavigation { get; set; }
        public virtual Tuyenduong? IdTuyenDuongNavigation { get; set; }
        public virtual ChuyendiThongtinphuongtien ChuyendiThongtinphuongtien { get; set; } = null!;
        public virtual ICollection<ChuyendiNhanvien> ChuyendiNhanviens { get; set; }
        public virtual ICollection<MuaVe> MuaVes { get; set; }
    }
}
