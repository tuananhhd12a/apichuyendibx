﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class HanhKhach
    {
        public HanhKhach()
        {
            MuaVes = new HashSet<MuaVe>();
        }

        public Guid IdHanhKhach { get; set; }
        public string? HoTen { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }

        public virtual ICollection<MuaVe> MuaVes { get; set; }
    }
}
